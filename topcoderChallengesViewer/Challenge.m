//
//  Challenge.m
//  topcoderChallengesViewer
//
//  Created by n1k1tung on 10/6/14.
//  Copyright (c) 2014 n1k1tung. All rights reserved.
//

#import "Challenge.h"


@implementation Challenge

@dynamic challengeName;
@dynamic currentPhaseName;
@dynamic challengeType;
@dynamic numRegistrants;
@dynamic numSubmissions;
@dynamic firstPlacePrize;
@dynamic totalPrize;
@dynamic currentPhaseRemainingTime;
@dynamic challengeID;

@end
