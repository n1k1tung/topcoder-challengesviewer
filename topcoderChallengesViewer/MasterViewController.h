//
//  MasterViewController.h
//  topcoderChallengesViewer
//
//  Created by n1k1tung on 10/4/14.
//  Copyright (c) 2014 n1k1tung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>


/*!
 *  @class MasterViewController
 *  @discussion challenges vc
 *
 *  @author n1k1tung
 *  @version 1.0
 */
@interface MasterViewController : UITableViewController

@end

/*!
 *  @class ChallengeCell
 *  @discussion challenge info cell
 *
 *  @author n1k1tung
 *  @version 1.0
 */
@interface ChallengeCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel* titleLabel;
@property (nonatomic, weak) IBOutlet UILabel* prizeLabel;
@property (nonatomic, weak) IBOutlet UILabel* typeLabel;
@property (nonatomic, weak) IBOutlet UILabel* timeLabel;
@property (nonatomic, weak) IBOutlet UILabel* phaseLabel;
@property (nonatomic, weak) IBOutlet UILabel* submissionsLabel;

@end