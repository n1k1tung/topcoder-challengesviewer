//
//  MasterViewController.m
//  topcoderChallengesViewer
//
//  Created by n1k1tung on 10/4/14.
//  Copyright (c) 2014 n1k1tung. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "RequestWrapper.h"
#import "WDActivityIndicator.h"
#import "DataStorage.h"
#import "AppDelegate.h"

typedef NS_ENUM(NSUInteger, ChallengeSegment) {
    ChallengeSegmentAssemblies,
    ChallengeSegmentF2FCode,
    ChallengeSegmentSwift,
    ChallengeSegmentMyChallenges,
    ChallengeSegmentCount
};

@interface MasterViewController () <NSFetchedResultsControllerDelegate>

@property (nonatomic, assign) ChallengeSegment currentChallengeSegment;

@property (nonatomic, strong) NSMutableArray *fetchedResultsControllers;

@end

@implementation MasterViewController 

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
//    self.navigationItem.leftBarButtonItem = self.editButtonItem;
//

    NSMutableArray* tempFetchControllersStub = [NSMutableArray new];
    for (int i = 0; i < ChallengeSegmentCount; ++i) {
        [tempFetchControllersStub addObject:[NSNull null]];
    }
    self.fetchedResultsControllers = tempFetchControllersStub;
    
    self.refreshControl = [UIRefreshControl new];
    [self.refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self refresh];
}

- (void)refresh
{
    WDActivityIndicator* activiyIndicator = [WDActivityIndicator new];
    activiyIndicator.center = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds));
    [self.view addSubview:activiyIndicator];
    [activiyIndicator startAnimating];
    
    NSDictionary* params = nil;
    
    switch (self.currentChallengeSegment) {
        case ChallengeSegmentAssemblies:
            params = @{@"platforms" : @"iOS", @"challengeType" : @"Assembly Competition", @"currentPhaseName" : @"Registration"};
            break;

        case ChallengeSegmentF2FCode:
            params = @{@"platforms" : @"iOS", @"challengeType" : @"(First2Finish OR Code)", @"currentPhaseName" : @"Registration"};
            break;
        
        case ChallengeSegmentSwift:
            params = @{@"technologies" : @"swift", @"currentPhaseName" : @"Registration"};
            break;
            
        case ChallengeSegmentMyChallenges:
            params = @{@"platforms" : @"iOS", @"-status" : @"(Completed OR Cancelled - Failed Screening)"};
            break;
            
        case ChallengeSegmentCount:
            NSAssert(NO, @"Shouldn't have come here");
            break;
    }
    
    [RequestWrapper requestWithType:TopcoderRequestTypeSearch params:params completionHandler:^(id response) {
        [DataStorage rewriteChallenges:response];
        [activiyIndicator removeFromSuperview];
        [self.refreshControl endRefreshing];
    } failureHandler:^(NSError *error) {
        NSLog(@"Failed to load challenges: %@", error);
        [activiyIndicator removeFromSuperview];
        [self.refreshControl endRefreshing];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSManagedObject *object = [[self fetchedResultsController] objectAtIndexPath:indexPath];
        [[segue destinationViewController] setDetailItem:object];
    }
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ChallengeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
        [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
            
        NSError *error = nil;
        if (![context save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (void)configureCell:(ChallengeCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Challenge *challenge = [self.fetchedResultsController objectAtIndexPath:indexPath];

    cell.titleLabel.text = challenge.challengeName;
    cell.phaseLabel.text = challenge.currentPhaseName;
    cell.typeLabel.text = challenge.challengeType;
    cell.submissionsLabel.text = [NSString stringWithFormat:@"R. %@ S. %@", challenge.numRegistrants, challenge.numSubmissions];
    cell.prizeLabel.text = [NSString stringWithFormat:@"1st: $%@ Rest: $%d", challenge.firstPlacePrize, [challenge.totalPrize intValue] - [challenge.firstPlacePrize intValue]];
    int time = [challenge.currentPhaseRemainingTime intValue];
    int days = time / 3600 / 24;
    int hours = (time - days * 24 * 3600) / 3600;
    cell.timeLabel.text = days? [NSString stringWithFormat:@"%dd %dh", days, hours] : [NSString stringWithFormat:@"%dh", hours];
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsControllers[self.currentChallengeSegment] != [NSNull null]) { // compare address since null is singleton
        return _fetchedResultsControllers[self.currentChallengeSegment];
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Challenge" inManagedObjectContext:[[AppDelegate sharedInstance] managedObjectContext]];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"currentPhaseRemainingTime" ascending:YES];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[[AppDelegate sharedInstance] managedObjectContext] sectionNameKeyPath:nil cacheName:@"Master"];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsControllers[self.currentChallengeSegment] = aFetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
	     // Replace this implementation with code to handle the error appropriately.
	     // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        [[[UIAlertView alloc] initWithTitle:@"FATAL ERROR" message:@"Try restarting the app" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
	}
    
    return _fetchedResultsControllers[self.currentChallengeSegment];
}    

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    if (controller != [self fetchedResultsController])
        return;

    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    if (controller != [self fetchedResultsController])
        return;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    if (controller != [self fetchedResultsController])
        return;
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:(ChallengeCell*)[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    if (controller != [self fetchedResultsController])
        return;
    
    [self.tableView endUpdates];
}

/*
// Implementing the above methods to update the table view in response to individual changes may have performance implications if a large number of changes are made simultaneously. If this proves to be an issue, you can instead just implement controllerDidChangeContent: which notifies the delegate that all section and object changes have been processed. 
 
 - (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    // In the simplest, most efficient, case, reload the table view.
    [self.tableView reloadData];
}
 */

#pragma mark - segments

- (IBAction)segmentChanged:(UISegmentedControl*)sender
{
    self.currentChallengeSegment = sender.selectedSegmentIndex;
    [self.tableView reloadData];
    [self refresh];
}

@end


@implementation ChallengeCell

@end