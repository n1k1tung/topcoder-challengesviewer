//
//  RequestWrapper.m
//
//
//  Created by n1k1tung on 30-9-10.
//  Copyright (c) 2014 topcoder. All rights reserved.
//

#import "RequestWrapper.h"
#import "Configuration.h"
#import <UIKit/UIKit.h>

@implementation RequestWrapper

/*!
 @discussion convinience constructor, initializes and starts the request
 */
+ (instancetype)requestWithType:(TopcoderRequestType)type params:(NSDictionary*)params
              completionHandler:(completionBlock)completion failureHandler:(failureBlock)failure
{
    RequestWrapper* wrapper = [RequestWrapper new];
    
    NSString* endpoint = type == TopcoderRequestTypeSearch? [Configuration topcoderSearchAPIEndpoint] : [Configuration topcoderChallengeAPIEndpoint];
    NSString* paramString = type == TopcoderRequestTypeSearch? [self generateParamString:params] : params[@"id"];
    
    NSString* urlString = [NSString stringWithFormat:@"%@%@", endpoint, paramString];
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL* url = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:@"GET"];

    wrapper.request = request;
    wrapper.completionBlock = completion;
    wrapper.failureBlock = failure;
    [wrapper start];
    return wrapper;
}

/*!
 *  Starts the service
 */
- (void)start
{
    _responseData = [[NSMutableData alloc] init];
    _connection = [[NSURLConnection alloc] initWithRequest:_request delegate:self];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

/*!
 *  Action on receive response
 *
 *  @param connection the connection
 *  @param response   the response
 */
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    _response = response;
    
}

/*!
 *  Action on received data
 *
 *  @param connection the connection
 *  @param data       the data
 */
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_responseData appendData:data];
}

/*!
 *  Action on failure
 *
 *  @param connection the connection
 *  @param error      error
 */
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    if (_failureBlock) {
        _failureBlock(error);
    }
}

/*!
 *  Action on connection finished
 *
 *  @param connection the connection
 */
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"Request finished...");
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    // done
    NSInteger statusCode = ((NSHTTPURLResponse *) _response).statusCode;
    if (statusCode != 200) {
        if (_failureBlock) {
            _failureBlock([self createErrorWithMessage:@"Unsuccessful response error" code:statusCode]);
        }
        return;
    }
    
    id responseResult = nil;
    if (_responseData) {
        responseResult = [NSJSONSerialization JSONObjectWithData:_responseData options:0 error:nil];
    }
    
    if (!responseResult) {
        // invalid data
        if (_failureBlock) {
            _failureBlock([self createErrorWithMessage:@"Invalid data" code:-1]);
        }
    } else {
        if (_completionBlock) {
            _completionBlock(responseResult);
        }
    }
}

#pragma mark - helpers

/*!
 *  Creates the error
 *
 *  @param message the error message
 *
 *  @return the error instance
 */
- (NSError *) createErrorWithMessage:(NSString *) message code:(NSInteger)code
{
    return [[NSError alloc] initWithDomain:@"RequestDomain" code:code userInfo:@{NSLocalizedDescriptionKey: message}];
}

/*!
 @discussion generates parameters string for search request
 @return the parameters string
 @param params params dictionary
 */
+ (NSString*)generateParamString:(NSDictionary*)params
{
    NSMutableString* result = [@"?q=" mutableCopy];
    BOOL first = YES;
    for (NSString* key in params) {
        first? [result appendFormat:@"%@:%@", key, params[key]] : [result appendFormat:@" AND %@:%@", key, params[key]];
        first = NO;
    }
    return result;
}

@end
