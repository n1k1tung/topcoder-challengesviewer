//
//  Configuration.h
//  eventApp
//
//  Created by n1k1tung on 01.10.14.
//  Copyright (c) 2014 Topcoder inc. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 @class Configuration
 @author n1k1tung
 @discussion represents class for build configurations
 */
@interface Configuration : NSObject

/*!
 @discussion tc search api endpoint url
 */
+ (NSString*)topcoderSearchAPIEndpoint;

/*!
 @discussion tc challenge api endpoint url
 */
+ (NSString*)topcoderChallengeAPIEndpoint;

/*!
 @discussion tc online review projects page
 */
+ (NSString*)topcoderOnlineReviewURL;

/*!
 @discussion tc challenge url
 */
+ (NSString*)topcoderChallengeURLFormat;

@end
