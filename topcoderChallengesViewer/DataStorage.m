//
//  DataStorage.m
//  topcoderChallengesViewer
//
//  Created by n1k1tung on 10/6/14.
//  Copyright (c) 2014 n1k1tung. All rights reserved.
//

#import "DataStorage.h"
#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <objc/runtime.h>

@implementation DataStorage

static NSString* const kChallengeEntity = @"Challenge";

/*!
 @return the moc for current thread
 */
+ (NSManagedObjectContext*)context
{
    return [[AppDelegate sharedInstance] managedObjectContext];
}

/*!
 @discussion Save the context.
 */
+ (NSError*)saveContext:(NSManagedObjectContext*)context
{
    NSError *error = nil;
    if (![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        NSLog(@"%s: Unresolved error %@, %@", __PRETTY_FUNCTION__, error, [error userInfo]);
    }
    return error;
}

+ (NSError*)rewriteChallenges:(NSArray*)challenges
{
    [self deleteTableWithName:kChallengeEntity];
    
    NSManagedObjectContext *context = self.context;
    
    for (NSDictionary* challenge in challenges) {
        Challenge *newManagedObject = [NSEntityDescription insertNewObjectForEntityForName:kChallengeEntity inManagedObjectContext:context];
        
        // If appropriate, configure the new managed object.
        newManagedObject.challengeID = @([challenge[@"_id"] integerValue]);
        newManagedObject.challengeName = challenge[@"_source"][@"challengeName"];
        newManagedObject.currentPhaseName = challenge[@"_source"][@"currentPhaseName"];
        newManagedObject.challengeType = challenge[@"_source"][@"challengeType"];
        newManagedObject.numRegistrants = @([challenge[@"_source"][@"numRegistrants"] integerValue]);
        newManagedObject.numSubmissions = @([challenge[@"_source"][@"numSubmissions"] integerValue]);
        newManagedObject.firstPlacePrize = @([challenge[@"_source"][@"firstPlacePrize"] integerValue]);
        newManagedObject.totalPrize = @([challenge[@"_source"][@"totalPrize"] integerValue]);
        newManagedObject.currentPhaseRemainingTime = @([challenge[@"_source"][@"currentPhaseRemainingTime"] integerValue]);
    }
 
    return [self saveContext:context];
}

+ (NSArray*)challenges
{
    return [self fetchEntity:kChallengeEntity sortedByKey:@"currentPhaseRemainingTime" ascending:YES];
}

#pragma mark - private

+ (NSArray *)fetchEntity:(NSString *)entityName
{
    return [self fetchEntity:entityName byAttribute:nil attributeValue:nil];
}

+ (NSArray *)fetchEntity:(NSString *)entityName sortedByKey:(NSString*)sortKey ascending:(BOOL)ascending
{
    return [self fetchEntity:entityName byAttribute:nil attributeValue:nil idOnly:NO sortedByKey:sortKey ascending:ascending];
}

+ (NSArray *)fetchEntity:(NSString *)entityName byAttribute:(NSString *)attributeName attributeValue:(id)attributeValue
{
    return [self fetchEntity:entityName byAttribute:attributeName attributeValue:attributeValue idOnly:NO];
}

+ (NSArray *)fetchEntity:(NSString *)entityName byAttribute:(NSString *)attributeName attributeValue:(id)attributeValue idOnly:(BOOL)idOnly
{
    return [self fetchEntity:entityName byAttribute:attributeName attributeValue:attributeValue idOnly:idOnly sortedByKey:nil ascending:NO];
}

+ (NSArray *)fetchEntity:(NSString *)entityName byAttribute:(NSString *)attributeName attributeValue:(id)attributeValue idOnly:(BOOL)idOnly sortedByKey:(NSString*)sortKey ascending:(BOOL)ascending
{
    NSAssert(entityName, @"Empty entity name in %s", __PRETTY_FUNCTION__);
    NSError* error = nil;
    NSFetchRequest* fetch = [[NSFetchRequest alloc] init];
    [fetch setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:self.context]];
    [fetch setIncludesPropertyValues:!idOnly];
    if ([attributeName length] && attributeValue)
        [fetch setPredicate:[NSPredicate predicateWithFormat:@"%K == %@", attributeName, attributeValue]];
    if (sortKey.length) {
        // Specify how the fetched objects should be sorted
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortKey ascending:ascending];
        [fetch setSortDescriptors:[NSArray arrayWithObjects:sortDescriptor, nil]];
    }
    
    return [self.context executeFetchRequest:fetch error:&error];
}

+ (NSError *)deleteTableWithName:(NSString *)tableName
{
    NSError* error = nil;
    NSFetchRequest* fetch = [[NSFetchRequest alloc] init];
    [fetch setEntity:[NSEntityDescription entityForName:tableName inManagedObjectContext:self.context]];
    [fetch setIncludesPropertyValues:NO];
    
    NSArray* table = [self.context executeFetchRequest:fetch error:&error];
    for (NSManagedObject* obj in table)
    {
        [self.context deleteObject:obj];
    }
    
    if (![self.context save:&error])
    {
        NSAssert(0, @"Failed to save context in %s, with error: %@", __PRETTY_FUNCTION__, error);
    }
    return error;
}

+ (void)mapFrom:(id)fromVar to:(id)toVar ignoreIvars:(NSArray *)ignoredIvars useReverseMapping:(BOOL)reverse // reverse mapping is needed to map from managed objects
{
    uint count = 0;
    objc_property_t *propertyList = NULL;
    Class classForMapping = reverse? [toVar class] : [fromVar class];
    NSArray* ivarsToIgnore = [@[@"hash", @"superclass", @"description", @"class", @"debugDescription"] arrayByAddingObjectsFromArray:ignoredIvars];
    
    while (classForMapping != [NSObject class])
    {
        propertyList = class_copyPropertyList(classForMapping, &count);
        for (uint i = 0; i < count; ++i)
        {
            NSString* propertyName = [NSString stringWithUTF8String:property_getName(propertyList[i])];
            if ([ivarsToIgnore containsObject:propertyName])
                continue;
            NSString* mappedPropertyName = nil;
            if ([propertyName isEqualToString:reverse ? @"iD" : @"ID"])
                mappedPropertyName = reverse ? @"ID" : @"iD";
            else if ([propertyName isEqualToString:reverse ? @"descr" : @"description"])
                mappedPropertyName = reverse ? @"description" : @"descr";
            else
                mappedPropertyName = propertyName;
            [toVar setValue:[fromVar valueForKey:propertyName] forKey:mappedPropertyName];
        }
        free(propertyList);
        classForMapping = [classForMapping superclass];
    }
    
}

+ (void)mapFrom:(id)fromVar to:(id)toVar ignoreIvars:(NSArray*)ignoredIvars {
    [self mapFrom:fromVar to:toVar ignoreIvars:ignoredIvars useReverseMapping:NO];
}

+ (void)mapFrom:(id)fromVar to:(id)toVar {
    [self mapFrom:fromVar to:toVar ignoreIvars:nil];
}

@end
