//
//  AppDelegate.h
//  topcoderChallengesViewer
//
//  Created by n1k1tung on 10/4/14.
//  Copyright (c) 2014 n1k1tung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

/*!
 *  @class AppDelegate
 *  @discussion the delegate responder for the application
 *
 *  @author n1k1tung
 *  @version 1.0
 */
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSArray *managedObjectContexts;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (AppDelegate*)sharedInstance;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
/*!
 @return moc for current thread
 */
- (NSManagedObjectContext*)managedObjectContext;


@end

