//
//  DataStorage.h
//  topcoderChallengesViewer
//
//  Created by n1k1tung on 10/6/14.
//  Copyright (c) 2014 n1k1tung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Challenge.h"

@interface DataStorage : NSObject

+ (NSManagedObjectContext*)context;

+ (void)mapFrom:(id)fromVar to:(id)toVar ignoreIvars:(NSArray *)ignoredIvars useReverseMapping:(BOOL)reverse;

+ (NSError*)rewriteChallenges:(NSArray*)challenges;
+ (NSArray*)challenges;

@end
