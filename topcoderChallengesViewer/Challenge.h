//
//  Challenge.h
//  topcoderChallengesViewer
//
//  Created by n1k1tung on 10/6/14.
//  Copyright (c) 2014 n1k1tung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Challenge : NSManagedObject

@property (nonatomic, retain) NSString * challengeName;
@property (nonatomic, retain) NSString * currentPhaseName;
@property (nonatomic, retain) NSString * challengeType;
@property (nonatomic, retain) NSNumber * numRegistrants;
@property (nonatomic, retain) NSNumber * numSubmissions;
@property (nonatomic, retain) NSNumber * firstPlacePrize;
@property (nonatomic, retain) NSNumber * totalPrize;
@property (nonatomic, retain) NSNumber * currentPhaseRemainingTime;
@property (nonatomic, retain) NSNumber * challengeID;

@end
