//
//  DetailViewController.m
//  topcoderChallengesViewer
//
//  Created by n1k1tung on 10/4/14.
//  Copyright (c) 2014 n1k1tung. All rights reserved.
//

#import "DetailViewController.h"
#import "RequestWrapper.h"
#import "Configuration.h"
#import "WDActivityIndicator.h"
#import "DataStorage.h"

@interface DetailViewController () {

    NSDateFormatter* _inDateFormatter;
    NSDateFormatter* _outDateFormatter;
}

@property (nonatomic, weak) IBOutlet UIWebView* descriptionView;
@property (nonatomic, weak) IBOutlet UILabel* registrationEndLabel;
@property (nonatomic, strong) IBOutletCollection(UILabel) NSArray* prizes;

@property (nonatomic, strong) NSDictionary* challengeInfo;

@property (nonatomic, strong) WDActivityIndicator* activityIndicator;

@end

@implementation DetailViewController

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem {
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        self.title = [_detailItem challengeName];
        
        WDActivityIndicator* activityIndicator = [WDActivityIndicator new];
        [activityIndicator startAnimating];
        self.activityIndicator = activityIndicator;
        
        [RequestWrapper requestWithType:TopcoderRequestTypeChallengeInfo params:@{@"id" : [_detailItem challengeID]} completionHandler:^(id response) {
            self.challengeInfo = response;
            // Update the view.
            [self configureView];
            [activityIndicator stopAnimating];
        } failureHandler:^(NSError *error) {
            NSLog(@"Failed to load challenge info: %@", error);
            [activityIndicator stopAnimating];
        }];
    }
}

- (void)configureView {
    // Update the user interface for the detail item.
    for (UILabel* prizeLabel in self.prizes) {
        prizeLabel.text = @"";
    }
    self.registrationEndLabel.text = @"";
    if (self.challengeInfo) {
        [self.descriptionView loadHTMLString:self.challengeInfo[@"detailedRequirements"] baseURL:nil];
        self.registrationEndLabel.text = [_outDateFormatter stringFromDate:[_inDateFormatter dateFromString:self.challengeInfo[@"registrationEndDate"]]];
        [self.challengeInfo[@"prize"] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            [self.prizes[idx] setText:[NSString stringWithFormat:@"$%d", [obj intValue]]];
            *stop = idx >= self.prizes.count-1;
        }];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _inDateFormatter = [NSDateFormatter new];
    _inDateFormatter.dateFormat = @"yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'sssZ";
    _outDateFormatter = [NSDateFormatter new];
    _outDateFormatter.dateStyle = NSDateFormatterMediumStyle;
    _outDateFormatter.timeStyle = NSDateFormatterMediumStyle;
    
    self.activityIndicator.center = CGPointMake(CGRectGetMidX(self.descriptionView.frame), CGRectGetMidY(self.descriptionView.frame));
    [self.view addSubview:self.activityIndicator];

    
    [self configureView];
    
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addToMine:)];
    self.navigationItem.rightBarButtonItem = addButton;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)registerTapped:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:[Configuration topcoderChallengeURLFormat], [_detailItem challengeID]]]];
}

- (IBAction)onlineReviewTapped:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[Configuration topcoderOnlineReviewURL]]];
}

- (void)addToMine:(id)sender
{
    
}

@end
