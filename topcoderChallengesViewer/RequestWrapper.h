//
//  RequestWrapper.h
//  
//
//  Created by n1k1tung on 30-9-10.
//  Copyright (c) 2014 topcoder. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef void (^completionBlock)(id response);
typedef void (^failureBlock)(NSError* error);

typedef NS_ENUM(NSUInteger, TopcoderRequestType) {
    TopcoderRequestTypeSearch,
    TopcoderRequestTypeChallengeInfo,
};

/*!
 *  @class RequestWrapper
 *  @discussion This is a silmple wrapper for NSURLRequest, also shows UIApplication network activity - expecting single request at a time
 *
 *  @author n1k1tung
 *  @version 1.0
 */
@interface RequestWrapper : NSObject

/*!
 *  Represent the request
 */
@property (nonatomic, strong) NSURLRequest* request;

/*!
 *  Represent the connection
 */
@property (nonatomic, strong) NSURLConnection* connection;

/*!
 *  Represent the response
 */
@property (nonatomic, strong) NSURLResponse* response;

/*!
 *  Represent the code block
 */
@property (nonatomic, copy) completionBlock completionBlock;

/*!
 *  Represent the failure block
 */
@property (nonatomic, copy) failureBlock failureBlock;

/*!
 *  Represent the response data
 */
@property (nonatomic, strong) NSMutableData* responseData;

/*!
 @discussion starts the connection
 */
-(void) start;

/*!
 @discussion convinience constructor, initializes and starts the request
 @param type request type of TopcoderRequestType
 @param params request params
 @param completion completion handler
 @param failure failure handler
 */
+ (instancetype)requestWithType:(TopcoderRequestType)type params:(NSDictionary*)params
             completionHandler:(completionBlock)completion failureHandler:(failureBlock)failure;

@end
