//
//  DetailViewController.h
//  topcoderChallengesViewer
//
//  Created by n1k1tung on 10/4/14.
//  Copyright (c) 2014 n1k1tung. All rights reserved.
//

#import <UIKit/UIKit.h>

/*!
 *  @class DetailViewController
 *  @discussion simple challenge description
 *
 *  @author n1k1tung
 *  @version 1.0
 */
@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;


@end

