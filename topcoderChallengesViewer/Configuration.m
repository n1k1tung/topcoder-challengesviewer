//
//  Configuration.m
//  eventApp
//
//  Created by n1k1tung on 01.10.14.
//  Copyright (c) 2014 Topcoder inc. All rights reserved.
//

#import "Configuration.h"

@implementation Configuration

static NSDictionary* config = nil;

+ (void)initialize
{
	NSData* plistData = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Configuration" ofType:@"plist"]];
	NSError* error = nil;
	config = [NSPropertyListSerialization propertyListWithData:plistData options:NSPropertyListImmutable format:nil error:&error];
	if (error) {
		NSLog(@"Couldn't initialize configurations: %@", error);
		config = nil;
	}
}

+ (NSString *)topcoderSearchAPIEndpoint
{
    return config[@"topcoderSearchAPIEndpoint"];
}

+ (NSString *)topcoderChallengeAPIEndpoint
{
    return config[@"topcoderChallengeAPIEndpoint"];
}

+ (NSString *)topcoderChallengeURLFormat
{
    return config[@"topcoderChallengeURLFormat"];
}

+ (NSString *)topcoderOnlineReviewURL
{
    return config[@"topcoderOnlineReviewURL"];
}

@end
